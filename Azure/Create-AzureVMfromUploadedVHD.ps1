


## Global
$rgName = "servers-rg"
$location = "westus"


## Network
$nicname = "SVRLVSUNAPP"
$subnet1Name = "Servers"
$subnetId = "/subscriptions/7b44d2cf-f7a7-4c69-b695-feaceadd6c4f/resourceGroups/servers-rg/providers/Microsoft.Network/virtualNetworks/SPAZ/subnets/Servers"
$vnetName = "SPAZ"

## Compute
$vmName = "SVRLVSUNAPP"
$computerName = "SVRLVSUNAPP"
$vmSize = "Standard_D1_v2"
$osDiskName = $vmName + "osDisk"
$DataDiskName = $vmName + "DataDisk"

## Network
$pip = New-AzureRmPublicIpAddress -Name $nicname -ResourceGroupName $rgName -Location $location -AllocationMethod Dynamic
$nic = New-AzureRmNetworkInterface -Name $nicname -ResourceGroupName $rgName -Location $location -SubnetId $subnetId -PublicIpAddressId $pip.Id

## Setup local VM object
$cred = Get-Credential
$vm = New-AzureRmVMConfig -VMName $vmName -VMSize $vmSize
$vm = Set-AzureRmVMOperatingSystem -VM $vm -Windows -ComputerName $computerName -Credential $cred -ProvisionVMAgent -EnableAutoUpdate

$vm = Add-AzureRmVMNetworkInterface -VM $vm -Id $nic.Id

$osDiskUri = "https://spaz01.blob.core.windows.net/spaz01/SVRLVSUNAPP-disk1.vhd"
$vm = Set-AzureRmVMOSDisk -VM $vm -Name $osDiskName -VhdUri $osDiskUri -CreateOption attach -Windows


$dataDiskUri = "https://spaz01.blob.core.windows.net/spaz01/SVRLVSUNAPP-disk2.vhd"
$vm = Add-AzureRmVMDataDisk -VM $vm -Name $DataDiskName -VhdUri $dataDiskUri -Lun 0 -CreateOption Attach -DiskSizeInGb $nul

## Create the VM in Azure
New-AzureRmVM -ResourceGroupName $rgName -Location $location -VM $vm -Verbose

